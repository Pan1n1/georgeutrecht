<div class="main">
        <h1>About us!</h1>
        <p>Most of our restaurants are designed by Amsterdam based Architecture & Design studio Framework, who signed for our bespoke interiors and sophisticated yet intimate ambiance,</p>
        <p>featuring typical George elements like metro tiles, lush leather couches, brass lighting, and marble bars. With each location having its own charm, your visit to George quite literally is a feast for your eyes and taste buds.</p>

        <div id="carouselExampleSlidesOnly" class="carousel slide" data-bs-ride="carousel">
          <div class="carousel-inner">
            <div class="carousel-item active">
              <img src="img/GeorgeKeuken.jpg" class="d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
              <img src="img/PersoneelGeorge.jpg" class="d-block w-100" alt="...">
            </div>
          </div>
        </div>

        <p>
             <br>
             Serving you iconic dishes and timeless classics with New York, Paris, the Côte d'Azur and Hong Kong in mind, along with local ingredients, freshly roasted coffee and signature cocktails, our restaurants are open all day, every day –  so come join us for an early morning espresso, a lazy Sunday brunch or an intimate dinner with friends in a relaxed, elegant atmosphere.
        </p>

    </div>