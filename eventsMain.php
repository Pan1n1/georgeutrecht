<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">

    <title>Book Event</title>
  </head>
  <body>


    <div class="main">
        <div class="mx-auto" style="width: 300px;">
            <h1>Book event at George Utrecht</h1>
        </div>       
        <div class="container-fluid mt-5">
            <div id="carouselExampleSlidesOnly" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                 <img src="img/verjaardag.jpg" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                 <img src="img/wedding.jpg" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                 <img src="img/Restaurant.jpg" class="d-block w-100" alt="saus">
                </div>
            </div>
            <br>

            <h4>
            Want to book an event at restaurant george? You're in the right place!
            Wether it be a wedding, a birthday or a work meeting. We do it all!
            </h4>
            <br>
            <a class="btn btn-default" href="./index.php?content=contact">Contact us »</a>
            <br>
            <a class="btn btn-default" href="./index.php?content=events">Leave your contact information »</a>
            </div>
        </div>

        <br><br><br>
        <img src="img/the-george.png" style="display: block;   margin-left: auto;   margin-right: auto;   width: 50%;  ">
    </div> <br><br><br>






   <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.3/dist/umd/popper.min.js" integrity="sha384-W8fXfP3gkOKtndU4JGtKDvXbO53Wy8SZCQHczT5FMiiqmQfUpWbYdTil/SxwZgAN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.min.js" integrity="sha384-skAcpIdS7UcVUC05LJ9Dxay8AXcDYfBJqt1CJ85S/CFujBsIzCIv+l9liuYLaMQ/" crossorigin="anonymous"></script>

  </body>
</html>
