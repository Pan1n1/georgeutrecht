<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">

    <title>Book Event</title>
  </head>
  <body>
<br>


    <div class="main">
        <div class="mx-auto" style="width: 300px;">
            <h1>Evenement boeken bij George Utrecht</h1>
        </div>       
        <div class="container-fluid mt-5">
          <div class="row">
            <div class="col-6 col-sm-6">
              <form action="./events_script.php" method="post">
                <div class="form-group" style="padding-left: 200px;">
        
                  <label for="inputEmail">Voornaam</label>
                  <input name="Voornaam" class="form-control" id="Voornaam" aria-describedby="VoornaamHelp" autofocus>
                  <small id="voornaamHelp" class="form-text text-muted">Verplicht</small>
                  <br>
        
                  <label for="inputEmail">Tussen voegsel</label>
                  <input name="Infix"  class="form-control" id="Infix" aria-describedby="InfixHelp" autofocus>
                  <br>
        
                  <label for="inputEmail">Achternaam</label>
                  <input name="Achternaam" type="Achternaam" class="form-control" id="Achternaam" aria-describedby="AchternaamHelp" autofocus>
                  <small id="emailHelp" class="form-text text-muted">Verplicht</small>
                  <br>
        
                  <label for="inputEmail">Mobiel</label>
                  <input name="Mobiel" type="Mobiel" class="form-control" id="Mobiel" aria-describedby="MobielHelp" autofocus>
                  <small id="emailHelp" class="form-text text-muted">Verplicht</small>
                  <br>

                  <label for="inputEmail">E-mail</label>
                  <input name="Email" type="Email" class="form-control" id="Email" aria-describedby="EmailHelp" autofocus>
                  <small id="emailHelp" class="form-text text-muted">Verplicht</small>
                  <br> <br> 



                  <br><br>
                  <button type="submit" class="btn btn-primary">Reserveer</button>
                </div>

              </form>

             
            </div>
            <div class="col-6 col-sm-6">
                    <img src="img/GeorgeLogo.png">
                  </div>
            <hr style="border-bottom: 1px solid black;">
            <br><br><br>
        
        </div> <br><br><br>






   <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.3/dist/umd/popper.min.js" integrity="sha384-W8fXfP3gkOKtndU4JGtKDvXbO53Wy8SZCQHczT5FMiiqmQfUpWbYdTil/SxwZgAN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.min.js" integrity="sha384-skAcpIdS7UcVUC05LJ9Dxay8AXcDYfBJqt1CJ85S/CFujBsIzCIv+l9liuYLaMQ/" crossorigin="anonymous"></script>

  </body>
</html>