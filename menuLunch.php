<div class="menuLunch">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="linenav"></div>
            </div>
            <div class="col-3 align-self-start">
                <div class="menuTitles">Sandwiches</div>
            </div>
            <div class="col">
                <div class="linenav"></div>
            </div>
        <div>    
    </div>
    <div class="gap-10"></div>
    <div class="container">
        <div class="row">
            <div class="col">
            <div class="card georgeFont" style="width: 21rem;">
                    <div class="card-body">
                      <h5 class="card-title">Carpacio</h5>
                        <h6 class="card-subtitle mb-2 text-muted">6 1/2</h6>
                        <p class="card-text">Broodje met goed vlees</p>
    	            </div>
                </div>
            </div>
            <div class="col">
            <div class="card georgeFont" style="width: 21rem;">
                    <div class="card-body">
                      <h5 class="card-title">Shoarma</h5>
                        <h6 class="card-subtitle mb-2 text-muted">7</h6>
                        <p class="card-text">Nog een broodje met goed vlees</p>
    	            </div>
                </div>
            </div>
            <div class="col">
            <div class="card georgeFont" style="width: 21rem;">
                    <div class="card-body">
                      <h5 class="card-title">Gezond</h5>
                        <h6 class="card-subtitle mb-2 text-muted">5</h6>
                        <p class="card-text">Broodje zonder goed vlees</p>
    	            </div>
                </div>
            </div>
        </div>
    </div>
    <div class="gap-40"></div>
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="linenav"></div>
            </div>
            <div class="col-3 align-self-start">
                <div class="menuTitles">Soepen</div>
            </div>
            <div class="col">
                <div class="linenav"></div>
            </div>
        <div>    
    </div>
    <div class="gap-10"></div>
    <div class="container">
        <div class="row">
            <div class="col">
            <div class="card georgeFont" style="width: 21rem;">
                    <div class="card-body">
                      <h5 class="card-title">Tomaat</h5>
                        <h6 class="card-subtitle mb-2 text-muted">15</h6>
                        <p class="card-text">Tomaten soep met ballen</p>
    	            </div>
                </div>
            </div>
            <div class="col">
            <div class="card georgeFont" style="width: 21rem;">
                    <div class="card-body">
                      <h5 class="card-title">Kip</h5>
                        <h6 class="card-subtitle mb-2 text-muted">10 1/2</h6>
                        <p class="card-text">Soep met andere ballen</p>
    	            </div>
                </div>
            </div>
            <div class="col">
            <div class="card georgeFont" style="width: 21rem;">
                    <div class="card-body">
                      <h5 class="card-title">Groenten</h5>
                        <h6 class="card-subtitle mb-2 text-muted">13</h6>
                        <p class="card-text">Soep zonder ballen</p>
    	            </div>
                </div>
            </div>
        </div>
    </div>     
</div>
<link rel="stylesheet" href="./css/menu.css">