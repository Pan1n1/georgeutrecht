<div class="coronaInfo">
    <div class="gap-40"></div>
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="linenav"></div>
            </div>
            <div class="col-3 align-self-start">
                <div class="menuTitles">General Rules</div>
            </div>
            <div class="col">
                <div class="linenav"></div>
            </div>
        <div>    
    </div>
    <div class="gap-10"></div>
    <div class="container">
        <div class="row">
            <div class="col">
            <div class="card georgeFont" style="width: 21rem;">
                    <div class="card-body">
                      <h5 class="card-title"><img src="./img/icons/qrCode.png" style="width: 50px;" alt="qrCode"></h5>
                        <p class="card-text">You will need a QR code with a vaccination, passed corona infection or negative test.</p>
    	            </div>
                </div>
            </div>
            <div class="col">
            <div class="card georgeFont" style="width: 21rem;">
                    <div class="card-body">
                      <h5 class="card-title"><img src="./img/icons/cough.png" style="width: 50px;" alt="cough"></h5>
                        <p class="card-text">In the case of sickness in the group, please contact us to cancel your reservation.</p>
    	            </div>
                </div>
            </div>
            <div class="col">
            <div class="card georgeFont" style="width: 21rem;">
                    <div class="card-body">
                      <h5 class="card-title"><img src="./img/icons/distance.png" style="width: 50px;" alt="distance"></h5>
                        <p class="card-text">Keep fitting distance from others, even from those who you came with.</p>
    	            </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="linenav"></div>
        <div class="gap-10"></div>
        <div class="georgeFont">For more information on the Dutch corona rules, please click <a href="https://www.government.nl/topics/coronavirus-covid-19/tackling-new-coronavirus-in-the-netherlands" target="_blank">here</a>.</div>
    </div>
</div>
<link rel="stylesheet" href="./css/menu.css">