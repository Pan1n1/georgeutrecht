drop database GeorgeUtrecht;
create database GeorgeUtrecht;
use GeorgeUtrecht;

create table user(
    ID int   NOT NULL AUTO_INCREMENT ,
    voornaam varchar(50) not null,
    infix varchar(50),
    achternaam varchar(50) not null,

    postcode varchar(10),
    huisnummer varchar(6),

    Mobiel varchar(10) null,
    email varchar(60) not null,
    verified Boolean not null default 0,

    role varchar(20),

    password varchar(60) not null,


    primary key(ID) );



  create table suplies(
	ID int(5),
	tables int(20),
    
    
    primary key(ID)
    );
 
    create table reservering(
    ID int   NOT NULL AUTO_INCREMENT ,
    voornaam varchar(50) not null,
    infix varchar(50),
    achternaam varchar(50) not null,
    mobiel varchar(10) not null,


    primary key(ID) );
    
    create table instructor(
    ID int   NOT NULL AUTO_INCREMENT ,
    voornaam varchar(50) not null,
    infix varchar(50),
    achternaam varchar(50) not null,

    postcode varchar(10),
    huisnummer varchar(6),

    Mobiel varchar(11) ,
    email varchar(60) not null,
    verified Boolean not null default 0,
    eigenaar Boolean not null default 0,

    proffesion varchar(20) not null,

	role varchar(20),


    password varchar(60) not null,


    primary key(ID) );

    create table docent(
    ID int   NOT NULL AUTO_INCREMENT ,
    voornaam varchar(50) not null,
    infix varchar(50),
    achternaam varchar(50) not null,

    postcode varchar(10),
    huisnummer varchar(6),

    Mobiel varchar(10),
    email varchar(60) not null,
    verified Boolean not null default 0,

    specialization varchar(20) not null,

	role varchar(20),

    password varchar(60) not null,


    primary key(ID) );

    create table student(
    ID int   NOT NULL AUTO_INCREMENT ,
    voornaam varchar(50) not null,
    infix varchar(50),
    achternaam varchar(50) not null,

    postcode varchar(10),
    huisnummer varchar(6),

    Mobiel varchar(10) ,
    email varchar(60) not null,
    verified Boolean not null default 0,

    opleiding varchar(20) not null,

    password varchar(60) not null,

	role varchar(20),
    
    leerlingenNummer int(20),
	klas varchar(20),
    
    primary key(ID) );

    CREATE TABLE IF NOT EXISTS `agenda` (
    `Day` date NOT NULL,
    `Chief bar` varchar(200),
    `Bartender` varchar(200),
    `Chief kitchen` varchar(200),
    `Cook` varchar(200),
    `Chief waiter` varchar(200),
    `Waiter` varchar(200),
    PRIMARY KEY (`Day`) 
    );
    
    create table cijferlijst( 
    klas varchar(20) not null,
    leerlingenNummer int(20) not null,
    naam varchar(60),
    praktijk float(5),
	nederlands float(5),
    burgerschap float(5)
    
    );
 
CREATE TABLE `bookings` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `date` date NOT NULL,
 `name` varchar(255) NOT NULL,
 `email` varchar(255) NOT NULL,
 `timeslot` VARCHAR(255) NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `contact`;
CREATE TABLE IF NOT EXISTS `contact` (
  `ID` int(20) NOT NULL AUTO_INCREMENT,
  `naam` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `afdeling` varchar(20) NOT NULL,
  `onderwerp` varchar(100) NOT NULL,
  `bericht` varchar(200) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;








 --  select ID, voornaam from student where postcode = ; 

    -- insert into Klant VALUES (NULL, 'Damian', '', 'Hijnen', 'DamianHijnen@hotmail.com', '062876578', '0',  'Inshallah');
		insert into suplies VALUES ('1', '20');
    --   insert into resevering VALUES (NULL, 'Damian', 'tabon', 'Hijnen', '0620446842' );

    --  insert into Student values (0,'Null','infix', 'zer0','4356ZV','67' );
    --  insert into Student values ( NULL ,'Damian', '', 'Hijnen','4205NC','5');


	

    select * from user order by ID desc;
	
    select * from bookings;
       
       select * from suplies;
      
      UPDATE suplies SET tables = 20 where id = 1;
      
      
	    insert into user VALUES (NULL, 'Jan', 'van', 'Klant', null, null, '062876578', 'klant@gmail.com', '1', 'klant', '$2y$10$ubdEKrTvShK.wrqWwly3gemzCdh/fdioH0.avfiOsGysb1YgkB3RG');
        insert into user VALUES (NULL, 'Jan', 'van', 'Klant', null, null, '062876578', 'docent@gmail.com', '1', 'docent', '$2y$10$ubdEKrTvShK.wrqWwly3gemzCdh/fdioH0.avfiOsGysb1YgkB3RG');
        insert into instructor VALUES (NULL, 'Jan', 'van', 'Klant', null, null, '062876578', 'eigenaar@georgeutrecht.nl', '1', '1', 'eigenaar', 'eigenaar','$2y$10$ubdEKrTvShK.wrqWwly3gemzCdh/fdioH0.avfiOsGysb1YgkB3RG');
        insert into student VALUES (NULL, 'Jan', 'van', 'Student', null, null, '062876578', 'student@student.mboutrecht.nl', '1', 'kok','$2y$10$ubdEKrTvShK.wrqWwly3gemzCdh/fdioH0.avfiOsGysb1YgkB3RG', 'student', '100001', '1ds4');
		insert into instructor VALUES (NULL, 'Jan', 'van', 'Klant', null, null, '062876578', 'werknemer@georgeutrecht.nl', '1', '0', 'kok', 'werknemer','$2y$10$ubdEKrTvShK.wrqWwly3gemzCdh/fdioH0.avfiOsGysb1YgkB3RG');
        insert into user VALUES (NULL, 'Jan', 'van', 'Klant', null, null, '062876578', 'admin@gmail.com', '1', 'admin', '$2y$10$ubdEKrTvShK.wrqWwly3gemzCdh/fdioH0.avfiOsGysb1YgkB3RG');
		insert into instructor VALUES (NULL, 'Jan', 'van', 'Klant', null, null, '062876578', 'begeleider@georgeutrecht.nl', '1', '0', 'kok', 'begeleider','$2y$10$ubdEKrTvShK.wrqWwly3gemzCdh/fdioH0.avfiOsGysb1YgkB3RG');

      

      insert into cijferlijst VALUES( '1k4' ,'100001', 'John Doe','6', '5', '5.6');
	  insert into cijferlijst VALUES( '1k4' ,'100002', 'Jane Doe','7', '6', '9');
	  insert into cijferlijst VALUES( '1k4' ,'100003', 'Johny Doe','9', '8.6', '9.3');
      
      insert into cijferlijst VALUES( '1k3' ,'100004', 'Daniel Wakka',null, null, null);

  --  select * from student where ID >= 2 AND ID <= 10 ;
  
  select * from instructor;
  
  select * from cijferlijst where klas ="1k4";
  
  select * from user;
  
  UPDATE cijferlijst set praktijk = "7" and nederlands = '5' where leerlingenNummer ="100003";
  