<div class="menuDiner">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="linenav"></div>
            </div>
            <div class="col-3 align-self-start">
                <div class="menuTitles">Pizza's</div>
            </div>
            <div class="col">
                <div class="linenav"></div>
            </div>
        <div>    
    </div>
    <div class="gap-10"></div>
    <div class="container">
        <div class="row">
            <div class="col">
            <div class="card georgeFont" style="width: 21rem;">
                    <div class="card-body">
                      <h5 class="card-title">Margherita</h5>
                        <h6 class="card-subtitle mb-2 text-muted">9</h6>
                        <p class="card-text">Pizza topped with tomato sauce & mozzarella</p>
    	            </div>
                </div>
            </div>
            <div class="col">
            <div class="card georgeFont" style="width: 21rem;">
                    <div class="card-body">
                      <h5 class="card-title">Tonno</h5>
                        <h6 class="card-subtitle mb-2 text-muted">10 1/2</h6>
                        <p class="card-text">Pizza topped with tomato sauce, mozzarella, tuna, caper & red union</p>
    	            </div>
                </div>
            </div>
            <div class="col">
            <div class="card georgeFont" style="width: 21rem;">
                    <div class="card-body">
                      <h5 class="card-title">Calzone</h5>
                        <h6 class="card-subtitle mb-2 text-muted">13</h6>
                        <p class="card-text">Folded pizza filled with tomato sauce, mozzarella, seasoned meat & spinash</p>
    	            </div>
                </div>
            </div>
        </div>
    </div>
    <div class="gap-40"></div>
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="linenav"></div>
            </div>
            <div class="col-3 align-self-start">
                <div class="menuTitles">Pasta's</div>
            </div>
            <div class="col">
                <div class="linenav"></div>
            </div>
        <div>    
    </div>
    <div class="gap-10"></div>
    <div class="container">
        <div class="row">
            <div class="col">
            <div class="card georgeFont" style="width: 21rem;">
                    <div class="card-body">
                      <h5 class="card-title">Carbonara</h5>
                        <h6 class="card-subtitle mb-2 text-muted">15</h6>
                        <p class="card-text">Spaghetti in a cheese and pancetta sauce with parmesan cheese</p>
    	            </div>
                </div>
            </div>
            <div class="col">
            <div class="card georgeFont" style="width: 21rem;">
                    <div class="card-body">
                      <h5 class="card-title">Lasagne</h5>
                        <h6 class="card-subtitle mb-2 text-muted">10 1/2</h6>
                        <p class="card-text">Lasagne with seasoned minced meat & béchamel</p>
    	            </div>
                </div>
            </div>
            <div class="col">
            <div class="card georgeFont" style="width: 21rem;">
                    <div class="card-body">
                      <h5 class="card-title">Calzone</h5>
                        <h6 class="card-subtitle mb-2 text-muted">13</h6>
                        <p class="card-text">Folded pizza filled with tomato sauce, mozzarella, seasoned meat & spinash</p>
    	            </div>
                </div>
            </div>
        </div>
    </div>     
</div>
<link rel="stylesheet" href="./css/menu.css">