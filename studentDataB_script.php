<?php
include("./functions.php");
include("./connect_db.php");
$submittedID = ($_POST['submittedID']);
$result = mysqli_query($conn, "SELECT `ID`, `voornaam`, `infix`, `achternaam`, `postcode`, `huisnummer`, `Mobiel`, `email`, `opleiding` FROM `student` WHERE ID = $submittedID");
$studentRecord = mysqli_fetch_assoc($result);
echo('<table class="table">
        <thead>
          <tr>
            <th scope="col">id</th>
            <th scope="col">voornaam</th>
            <th scope="col">tussenvoegsel</th>
            <th scope="col">achternaam</th>
            <th scope="col">postcode</th>
            <th scope="col">huisnummer</th>
            <th scope="col">mobiel</th>
            <th scope="col">email</th>
            <th scope="col">opleiding</th>
          </tr>
        </thead>
        <tbody>
        <tr>
          <td>'.$studentRecord["ID"].'</td>
          <td>'.$studentRecord["voornaam"].'</td>
          <td>'.$studentRecord["infix"].'</td>
          <td>'.$studentRecord["achternaam"].'</td>
          <td>'.$studentRecord["postcode"].'</td>
          <td>'.$studentRecord["huisnummer"].'</td>
          <td>'.$studentRecord["Mobiel"].'</td>
          <td>'.$studentRecord["email"].'</td>
          <td>'.$studentRecord["opleiding"].'</td>
        </tr>
      </tbody>
    </table>
    <div class="container">
        <div class="row">
            <div class="col-2"></div>
            <div class="col-4">
                <a href="./index.php?content=./studentDataB" class="btn btn-warning btn-lg btn-block" role="button">Terug</a>
            </div>
            <div class="col-4">
                <a href="./index.php?content=./home-b" class="btn btn-warning btn-lg btn-block" role="button">Home page</a>
            </div>
            <div class="col-4"></div>
        </div>
    </div>
');