<div class="row">
        <div class="col-4">
          <h6>
            <small>Copyright 2021 Maison George</small>
          </h6>
        </div>
        <div class="col-4 text-center">
          <h6>
            <small>Terms & Privacy</small>
          </h6>
        </div>
        <div class="col-4 text-right">
          <h6>
            <small>Made by StadsPark bv.</small>
          </h6>
        </div>
</div>