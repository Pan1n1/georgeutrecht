<?php
  function sanitize($raw_data) {
    global $conn;
    $data = htmlspecialchars($raw_data);
    $data = mysqli_real_escape_string($conn, $data);
    $data = trim($data);
    return $data;
  }

  function mk_password_hash_from_microtime() {
    $mut = microtime();

    $time = explode(" ", $mut);

    $password = $time[1] * $time[0] * 1000000;

    $password_hash = password_hash($password, PASSWORD_BCRYPT);

    $onehour = mktime(1,0 ,0, 1, 1, 1970);

    $date_formated = date("d-m-Y", ($time[1] + $onehour));

    $time_formated = date("H:i:s", ($time[1] + $onehour));

    return array("password_hash" => $password_hash,
                 "date"          => $date_formated,
                 "time"          => $time_formated);
  }


  function is_authorized($userroles) {
    if (!isset($_SESSION["id"])) {
      return header("Location: ./index.php?content=message&alert=auth-error");
    } elseif ( !in_array($_SESSION["userrole"], $userroles)) {
      return header("Location: ./index.php?content=message&alert=auth-error-user");
    } else {
      return true;
    }
  }

  
  function error() {
    global $conn;
    //  $mysqli = new mysqli("localhost","my_user","my_password","my_db");
     echo("Error description: " . $conn -> error);
    }

        
  function tabon($ts, $bookings){
    $mysqli = new mysqli('localhost', 'root', '', 'georgeutrecht');

    $status = false;
   
      $date = $_GET['date'];
      
      
      
      $stmt = $mysqli->prepare("select * from bookings where date = ? and timeslot = ?");
      $stmt->bind_param('ss', $date, $timeslot);
      
      
      
      if(in_array($ts, $bookings)){
          $status = true;
          $result = $stmt->get_result();
          //Checked of tafel beschikbaar is

      }
      return $status;
  }


  function userrole($role) {
    switch ($role) { 
      case 'docent':
        header("Location: ./index.php?content=home-d");
        break;
      case 'eigenaar':
        header("Location: ./index.php?content=home-e");
        break;
      case 'student':
        header("Location: ./index.php?content=home-s");
        break;
      case 'werknemer':
          header("Location: ./index.php?content=home-w");
          break;
      case 'begeleider':
        header("Location: ./index.php?content=home-b");
        break;
      case 'klant':
        header("Location: ./index.php?content=home-k");
        break;
      case 'admin':
        header("Location: ./index.php?content=home-a");
        break;
      default:
        header("Location: ./index.php?content=home");
        break;
    }
  }

  function login($email,$password,$result){

    if (!mysqli_num_rows($result)) {
      // E-mailadres onbekend...
      header("Location: ./index.php?content=login&message=email-unknown");
    } else {
  
      $record = mysqli_fetch_assoc($result);
  
      // var_dump((bool) $record["activated"]);
  
      if (!$record["verified"]) {
        // Not activated
        header("Location: ./index.php?content=login&message=not-activated&email=$email");
      } elseif (!password_verify($password, $record["password"])) {
        // No password match
        header("Location: ./index.php?content=login&message=no-pw-match&email=$email");
      } else {
        // password matched
        var_dump($record["role"]); 
  
        $_SESSION["email"] = $record["email"];
        $_SESSION["password"] = $record["password"];
        $_SESSION["userrole"] = $record["role"];
        // var_dump($record["rol"]);exit();
        switch ($_SESSION["userrole"]) { 
          case 'docent':
            header("Location: ./index.php?content=home-d");
            break;
          case 'eigenaar':
            header("Location: ./index.php?content=home-e");
            break;
          case 'student':
            header("Location: ./index.php?content=home-s");
            break;
          case 'werknemer':
              header("Location: ./index.php?content=home-w");
              break;
          case 'begeleider':
            header("Location: ./index.php?content=home-b");
            break;
          case 'klant':
            header("Location: ./index.php?content=home-k");
            break;
          case 'admin':
            header("Location: ./index.php?content=home-a");
            break;
          default:
            header("Location: ./index.php?content=home");
            break;
        } 
      }
    }
  }



 //
 ?>
 