
<div class="container">
    <div class="georgeFont">
        <div class="row">
            <div class="col">
                <img src="./img/fooood-at-the-food-department.jpg" alt="foods">
            </div>
            <div class="col">
                <form action="./index.php?content=./activationPages/activateKlant_script" method="post">
                    <div class="row">
                        <div class="col-8">
                            <div class="form-group"> 
                            <label for="firstname">First name</label>
                            <input name="firstname" type="text" class="form-control" id="firstname"  aria-describedby="firstnameHelp" required>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group"> 
                            <label for="infix">Infix</label>
                            <input name="infix" type="text" class="form-control" id="infix"  aria-describedby="infixHelp">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group"> 
                            <label for="lastname">Last name</label>
                            <input name="lastname" type="text" class="form-control" id="lastname"  aria-describedby="lastnameHelp" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group"> 
                            <label for="inputPassword">Password:</label>
                            <input name="password" type="password" class="form-control" id="inputPassword" aria-describedby="passwordHelp" requried>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-4">
                            <div class="form-group"> 
                            <label for="mobiel">* Telephone number:</label>
                            <input name="mobiel" type="text" class="form-control" id="mobiel" aria-describedby="mobielHelp">
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group"> 
                            <label for="mobiel">* Zip code:</label>
                            <input name="zipcode" type="text" class="form-control" id="zipcode" aria-describedby="zipcodehelp">
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group"> 
                            <label for="mobiel">* House number:</label>
                            <input name="housenumber" type="text" class="form-control" id="housenumber" aria-describedby="housenumberHelp">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <input type="hidden" name="email" value="<?php echo($_GET["email"]) ?>">
                        <button type="submit" class="btn btn-warning btn-lg btn-block">Activate</button> 
                        <div id="starHelp" class="form-text">* = Optional</div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


