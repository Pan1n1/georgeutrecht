
   
   <main>

   
   <div class="container">
      <div class="row mt-3">
        <div class="col-md-3 text-center">
          <h4 class="heading_2">
            OPENING<br>
            HOURS<br><br>
          </h4 class="about_text">
          OPEN DAILY FROM<br>
          11A.M. TILL LATE<br><br>
          <small><em>We'll rock your boat at George Marina</em></small><br><br>
          <small><em>On sunny days we have valet parking for a fee of <u>€12,50</u> during lunch and <u>€15</u> during dinner</em></small>
          <hr style="border-bottom: 1px solid black;">
          <img src="https://uploads-ssl.webflow.com/5f3ece93689659d6e7431728/60d1aff9974cae25741692a3_marina.jpg" loading="lazy" sizes="(max-width: 479px) 200px, (max-width: 767px) 39vw, (max-width: 991px) 200px, (max-width: 1919px) 19vw, 200px" height="140" srcset="https://uploads-ssl.webflow.com/5f3ece93689659d6e7431728/60d1aff9974cae25741692a3_marina-p-500.jpeg 500w, https://uploads-ssl.webflow.com/5f3ece93689659d6e7431728/60d1aff9974cae25741692a3_marina-p-800.jpeg 800w, https://uploads-ssl.webflow.com/5f3ece93689659d6e7431728/60d1aff9974cae25741692a3_marina.jpg 938w" alt="" class="image-12">
          <b><a href="./index.php?content=AboutUs" style="color:Black">ABOUT US</a></b><br><br>
          <a href="https://www.google.com/maps/place//data=!4m2!3m1!1s0x47c60bd4c75894bd:0xcdcb9a745b29464d?source=g.page.share" target="_blank" style="color:#212529">MAPS</a><br>
         <a href="mailto:hello@georgemarina.nl" style="color:Black">E-MAIL </a>
          <br>
         <a href="tel:+31207370280" style="color:Black">PHONE</a>
          <br>
         <a href="index.php?content=contact" style="color:Black">CONTACT</a>
          </h4>
        </div>

        <div class="col-md-6 text-center border-black-left border-black-right">
          <h6 class="Heading_about">
            THIS IS US<br><br><br>
            George Marina is set in a spectacular, three storey waterside building in the upcoming Amstelkwartier, right along the banks of the Amstel river that flows into Amsterdam's iconic canals.
            <br>
            <br>
            With large windows overlooking the marina in front, an open kitchen and sushi counter, lush plants, a striking 360 green marble bar and an eye-catching staircase right in the middle of the restaurant, it's also our cuisine that will leave a long lasting impression. Being so close to the water, we're famous for our seafood and sushi platters, but you might as well order steak frites, a juicy bistro burger or a veggie dish. A great all-day hangout for breakfast, lunch, dinner or Friday night cocktails accompanied by a live set from our resident DJ.
            <br>
            <br>
            Despite our restaurants' modern allure, our large, sunny terrace has more of a Côte d'Azur feel – especially in the summer when guests coming from the canals dock their boats in the marina, and enjoy a drink while watching the sunset. No matter what season and what time of the day though, George Marina is a great place to escape hectic, daily life – if only for one day.
            <br>
            <br>
            - fin -
          </h6>
        </div>
        <div class="col-md-3 text-center">
         <h6><?php if(isset($_SESSION["userrole"])){echo ("<a href='./logout.php'>Logout </a>"); } else { echo("<a href='./index.php?content=register'>Sign up   </a>");}  if(isset($_SESSION["userrole"])){echo ("<a href='./index.php?content=login'>My account</a>"); } else { echo("<a href='./index.php?content=login'>Login</a>");} ?>  </h6>
        
        <h2>
              
          <hr style="border-bottom: 1px solid black;">
            MENU'S
            <br>
            <br>
            <br>
           <a href="./index.php?content=menuDiner" style="color: black"> DINNER </a>
            <br>
           <a href="./index.php?content=menuLunch" style="color: black"> LUNCH </a>
            <br>
            <br>
            <br>
            <br>
            <hr style="border-bottom: 1px solid black;">
            <br>
           <a href="./table_script.php" style="color: black"> <b>BOOK</b>
            <br>
            <b>MY</b>
            <br>
            <b>TABLE</b> </a>

            <br><br><a href="./index.php?content=coronaInfo"><h2>Corona</h2></a>
          </h2>
        </div>
      </div>
      <hr style="border-bottom: 1px solid black;">


      <div class="row">


        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel" data-interval="5000">
          <div class="carousel-inner">
            <div class="carousel-item active">
              <img class="d-block w-100" src="https://uploads-ssl.webflow.com/5f3ece93689659d6e7431728/60d5d8dd22d367398f572580_GEORGE%20MARINA23x-p-1600.jpeg" alt="First slide">
            </div>
            <div class="carousel-item">
              <img class="d-block w-100" src="https://uploads-ssl.webflow.com/5f3ece93689659d6e7431728/612657d7a3552cbc46c1702a_Untitled%2011%402x-p-1600.jpeg" alt="Second slide">
            </div>
            <div class="carousel-item">
              <img class="d-block w-100" src="https://uploads-ssl.webflow.com/5f3ece93689659d6e7431728/60d5d8dc23267d1361257ea7_GEORGE%20MARINA18x-p-1600.jpeg" alt="Third slide">
            </div>
            <div class="carousel-item">
              <img class="d-block w-100" src="https://uploads-ssl.webflow.com/5f3ece93689659d6e7431728/60d5d8dea725947dd46a1731_GEORGE%20MARINA49x-p-1600.jpeg" alt="Fourth slide">
            </div>
            <div class="carousel-item">
              <img class="d-block w-100" src="https://uploads-ssl.webflow.com/5f3ece93689659d6e7431728/611152b7638de16073131e92_Marina%20Terras%402x-p-1600.jpeg" alt="Fifth slide">
            </div>
            <div class="carousel-item">
              <img class="d-block w-100" src="https://uploads-ssl.webflow.com/5f3ece93689659d6e7431728/60d5d8da23267d0ffc257ea5_GEORGE%20MARINA08x-p-1600.jpeg" alt="Sixth slide">
            </div>
          </div>
          <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>


      </div>
      <hr style="border-bottom: 1px solid black;">


      <div class="row">
        <div class="col-md-3 text-center">
          <h3>
            BOOK EVENT<br><br>
            <a href="./index.php?content=eventsMain" style="color:Black">MORE INFORMATION</a><br><br>
          </h3>
          <h5>
            <small>In search of an inspiring, creative meeting space that's all about quality, comfort and a relaxed atmosphere? We're here to make it happen for you.</small>
          </h5>
          <hr style="border-bottom: 1px solid black;">
          <h1>
            <br>
            <br>
            <a href="./index.php?content=Career" style="color:Black">CAREERS</a>
          </h1>
        </div>
        <div class="col-6">
          <img src="https://uploads-ssl.webflow.com/5f3ece93689659d6e7431728/610a64e7106fb70c4dfe3a1d_marina.gif" height="500" width="600">
        </div>
        <div class="col-md-3 text-center">
          <img src="https://uploads-ssl.webflow.com/5f3ece93689659d6e7431728/60d1af819d9d3d0129c0631e_Champagne.png" loading="lazy" height="400">
          <br>
          <br>
          <br>
          <h6>
            "COME QUICKLY, I’M TASTING THE SUNSET AND THE STARS"<br><br><br><br>
            <small>- DOM PERIGNON -</small>
          </h6>
        </div>
      </div>
      <hr style="border-bottom: 1px solid black;">




    </div>


  </main>



  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  <script src="./js/app.js"></script>
</body>

</html>