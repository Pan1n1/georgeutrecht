<div class="container">
  <div class="row">
    <div class="col3">
      <div class="card" style="width: 18rem;">
        <img class="card-img-top" src="./img/icons/mailIcon.png" alt="Card image cap">
        <div class="card-body">
          <h5 class="card-title">Student data</h5>
          <p class="card-text">Vraag alle data op van een student naar wens</p>
          <a href="./index.php?content=./studentDataB" class="btn btn-primary">go</a>
        </div>
      </div>
    </div>
    <div class="col3">
      <div class="card" style="width: 18rem;">
        <img class="card-img-top" src="./img/icons/mailIcon.png" alt="Card image cap">
        <div class="card-body">
          <h5 class="card-title">agenda</h5>
          <p class="card-text">Zie wie er wanneer staat voor de aankomende week</p>
          <a href="./index.php?content=./agenda" class="btn btn-primary">go</a>
        </div>
      </div>
    </div>
    <div class="col3"></div>
    <div class="col3"></div>
  </div>
</div>

