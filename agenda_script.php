<?php
include("./connect_db.php");
include("./functions.php");
$result = mysqli_query($conn, "SELECT CURRENT_DATE");
$result2 = mysqli_fetch_assoc($result);
$currentdate = $result2["CURRENT_DATE"];
$year = intval(substr($currentdate, 0, 4));
$month = intval(substr($currentdate, 5, 2));
$day = intval(substr($currentdate, -2));
if (isset($currentdate)) {
    if ($month = 1 || 3 || 5 || 7 || 8 || 10) {
        if ($day + 7 > 31) {
            $month = $month + 1;
            $day = $day + 7 - 31;
        } else {
            $day = $day + 7;
        }
    } elseif ($month = 2) {
        if ($day + 7 > 28) {
            $month = $month + 1;
            $day = $day + 7 - 28;
        } else {
            $day = $day + 7;
        }
    } elseif ($month = 12) {
        if ($day + 7 > 31) {
            $year = $year + 1;
            $month = 1;
            $day = $day + 7 - 31;
        } else {
            $day = $day + 7;
        }
    } else {
        if ($day + 7 > 30) {
            $month = $month + 1;
            $day = $day + 7 - 30;
        } else {
            $day = $day + 7;
        }
    }
}
$futuredate = $year . "-" . $month . "-" . $day;
$sql = "INSERT INTO `agenda`(`Day`) VALUES ('" . $futuredate . "');";
$sql2 = "DELETE FROM `agenda` WHERE Day < CURRENT_DATE";
mysqli_query($conn, $sql);
mysqli_query($conn, $sql2);
header("location: ./index.php?content=agenda&first=false");
?>