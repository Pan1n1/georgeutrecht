<?php
if (empty($_POST["email"])) {
    header("Location: ./index.php?content=message&alert=no-email");
} else {
    include("./connect_db.php");
    include("./functions.php");

    $email = sanitize($_POST["email"]);

    $sql = "SELECT * FROM `user` WHERE `email` = '$email'";

    $result = mysqli_query($conn, $sql);

    if (substr($email,-22) == "@student.mboutrecht.nl"){
      $detectedRole = "Student";
    } elseif (substr($_POST["email"],-14) == "@mboutrecht.nl") {
      $detectedRole = "Docent";
    } elseif ($_POST["email"] == "georgemarina@georgeutrecht.nl"){
      $detectedRole = "Eigenaar";
    } elseif (substr($_POST["email"],-17) == "@georgeutrecht.nl"){
      $detectedRole = "Begeleider";
    } else {
      $detectedRole = "Klant";
    }




    if (mysqli_num_rows($result)) {
        // Melding email bestaat
        header("Location: ./index.php?content=register&message=emailexists");
    } else {
        // echo $sql;exit();
        if (mysqli_query($conn, $sql)) {

            $id = mysqli_insert_id($conn);
            
            // email versturen
            $to = $email;
            $subject = "Activatie link voor uw account van georgestadspark.nl";
            $message = '<!doctype html>
                <html lang="en">
                  <head>
                    <!-- Required meta tags -->
                    <meta charset="utf-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1">
                
                    <!-- Bootstrap CSS -->
                    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
                    <style>
                        body {
                        font-size: 1.3em;
                        font-family: Baskerville;
                    }
                    </style>
                    <title>Hello, world!</title>
                  </head>
                  <body>
                    <h1></h1>
                    <h2>Beste ' . $detectedRole . ',</h2>
                    <p>U heeft zich onlangs geregistreerd voor de site www.georgestadspark.nl</p>
                    <p>Klik <a href="http://www.georgestadspark.nl/index.php?content=./activationPages/activate' . $detectedRole . '&email=' . $email . '">hier</a> voor het activeren van uw acount</p> 

                    <p>placeholder1 <a href="http://www.georgeutrecht.com//index.php?content=./activationPages/activate' . $detectedRole . '&email=' . $email . '">hier</a> voor het activeren van uw acount</p> 

                    <p>Bedankt voor het registreren</p>
                    <p>Met vriendelijke groet,</p>
                    <p>Staff George Stadspark</p>
                    <p>CEO </p>

                    <!-- Optional JavaScript; choose one of the two! -->

                    <!-- Option 1: Bootstrap Bundle with Popper -->
                    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
                
                    <!-- Option 2: Separate Popper and Bootstrap JS -->
                    <!--
                    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js" integrity="sha384-KsvD1yqQ1/1+IA7gi3P0tyJcT3vR+NdBTt13hSJ2lnve8agRGXTTyNaBYmCR/Nwi" crossorigin="anonymous"></script>
                    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.min.js" integrity="sha384-nsg8ua9HAw1y0W1btsyWgBklPnCUAFLuTMS2G72MMONqmOymq585AcH49TLBQObG" crossorigin="anonymous"></script>
                    -->
                  </body>
                </html>';

            $headers = "MIME-Version: 1.0\r\n";
            $headers .= "Content-type: text/html; charset=UTF-8\r\n";
            $headers .= "From: Admin@mbostadspark.nl\r\n";
            $headers .= "Cc: moderator@mbostadspark.nl";
            $headers .= "Bcc: moderator@mbostadspark.nl";
            
            mail($to, $subject, $message, $headers);

            
          header("Location: ./index.php?content=register&message=register-success");
        } else {
            // error melding
            header("Location: ./index.php?content=register&message=register-error");
        }
    }
}
?>