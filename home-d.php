<style>
    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    td, th {
        border: 1px solid #838383;
        text-align: left;
        padding: 8px;
    }

    tr:nth-child(even) {
        background-color: #dddddd;
    }
</style>

Docent home
<?php
include("./connect_db.php");
include("./functions.php");



function c1k4($conn){
    $sql = 'SELECT * from cijferlijst where klas ="1k4";';

    $result = mysqli_query($conn, $sql);
   

    if ($result->num_rows > 0) {
        // output data of each row
        echo("<table><tr><td>*</td><td>klas</td><td>Leerlingen nummer</td><td>naam</td><td>praktijk</td><td>Nederlands</td><td> Burgerschap</td></tr>");
        while($row = $result->fetch_assoc()) {
            echo ( "<tr><td><a href='./index.php?content=gradeAdjust&leerling=" . $row["leerlingenNummer"]. "&praktijk=" . $row["praktijk"] . "&nederlands=" . $row["nederlands"]   . "&burgerschap=" . $row["burgerschap"]  ."'>edit</a></td><td>" . $row["klas"] . "</td><td>" . $row["leerlingenNummer"] . "</td><td>" . $row["naam"] . "</td><td>" . $row["praktijk"] . "</td><td>" . $row["nederlands"] . "</td><td>" . $row["burgerschap"] . "</td></tr>");
        }  
        echo("</table>");
    } else {
        echo "0 results";
        }

}

function c1k3($conn){
    $sql = 'SELECT * from cijferlijst where klas ="1k3";';

    $result = mysqli_query($conn, $sql);
   

    if ($result->num_rows > 0) {
        // output data of each row
        echo("<table><tr><td>*</td><td>klas</td><td>Leerlingen nummer</td><td>naam</td><td>praktijk</td><td>Nederlands</td><td> Burgerschap</td></tr>");
        while($row = $result->fetch_assoc()) {
            echo ( "<tr><td><a href='./index.php?content=gradeAdjust&leerling=" . $row["leerlingenNummer"]. "&praktijk=" . $row["praktijk"] . "&nederlands=" . $row["nederlands"]   . "&burgerschap=" . $row["burgerschap"]  ."'>edit</a></td><td>" . $row["klas"] . "</td><td>" . $row["leerlingenNummer"] . "</td><td>" . $row["naam"] . "</td><td>" . $row["praktijk"] . "</td><td>" . $row["nederlands"] . "</td><td>" . $row["burgerschap"] . "</td></tr>");
        }  
        echo("</table>");
    } else {
        echo "0 results";
        }
}




?>
<br><br>




<div class="container">
  <h2>Cijfers</h2>
  <p>Kies de klas waar je de cijfers van wil zien</p>
   <!-- De Inklapbare lijst 1CK3 -->
   <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo">1K3</button>
  <div id="demo" class="collapse">
    <?php c1k3($conn); ?>
        
  </div>

  <br><br>

  <!-- De Inklapbare lijst -->
  <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo">1K4</button>
  <div id="demo" class="collapse">
    <?php c1k4($conn); ?>
  </div>

</div>
